﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {

        private readonly IRepository<Preference> _preferenceRep;

        public PreferencesController(IRepository<Preference> preferenceRep)
        {
            _preferenceRep = preferenceRep;
        }

        /// <summary>
        /// Get all prefernces
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetPreferncesAsync()
        {
            var prefernces = await _preferenceRep.GetAllAsync();
            var preferncesResponse = prefernces.Select(prefernce => new PreferenceResponse
            {
                Id = prefernce.Id,
                Name = prefernce.Name
            }).ToList();
            return Ok(preferncesResponse);
        }

        // GET api/<PreferencesController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            throw new NotImplementedException();
        }

        // POST api/<PreferencesController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
            throw new NotImplementedException();
        }

        // PUT api/<PreferencesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
            throw new NotImplementedException();
        }

        // DELETE api/<PreferencesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
