﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IRepository<PromoCode> _promoCodeRep;
        private readonly IRepository<Preference> _preferenceRep;
        private readonly IRepository<Customer> _customerRep;

        public PromocodesController(
            IRepository<PromoCode> promoCodeRep,
            IRepository<Preference> preferenceRep,
            IRepository<Customer> customerRep
            )
        {            
            _promoCodeRep = promoCodeRep;
            _preferenceRep = preferenceRep;
            _customerRep = customerRep;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodeRep.GetAllAsync();
            var promocodesToShort = promocodes.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToString(),
                EndDate = x.EndDate.ToString(),
                PartnerName = x.PartnerName
            }
            ).ToList();
            return Ok(promocodesToShort);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferenceRep.GetAllAsync();
            var preference = preferences.ToList().Find(x => x.Name == request.Preference);

            var promocode = new PromoCode()
            {
                Code = request.PromoCode,
                Preference = preference,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(1),
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo
            };

            await _promoCodeRep.CreateAsync(promocode);

            var customers = await _customerRep.GetAllAsync();
            var filteredCustomers = customers.Where(x => x.CustomerPreferences.Select(a => a.PreferenceId).Any(b => b == preference.Id)).ToList();

            foreach (var customer in filteredCustomers)
            {

                customer.Promocodes.Add(promocode);
                await _customerRep.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}