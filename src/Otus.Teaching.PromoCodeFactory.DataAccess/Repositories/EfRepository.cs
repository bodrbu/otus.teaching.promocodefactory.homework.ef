﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly AppDBContext _db;

        public EfRepository(AppDBContext appContext)
        {
            _db = appContext;
        }

        public async Task CreateAsync(T item)
        {
            T dbItem = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == item.Id);
            if (dbItem != null) throw new ArgumentException($"The object with ID '{item.Id}' already exists");
            await _db.Set<T>().AddAsync(item);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            T item = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if (item == null) throw new ArgumentException($"The object with ID '{id}' does not exist");
            _db.Set<T>().Remove(item);
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _db.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var item = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            return item;
        }

        public async Task<List<T>> GetListByListId(List<Guid> ListId)
        {
            return await _db.Set<T>().Where(x => ListId.Contains(x.Id)).ToListAsync();
        }

        public async Task UpdateAsync(T item)
        {
            T dbItem = await _db.Set<T>().FindAsync(item.Id);
            if (dbItem == null) throw new ArgumentException($"The object with ID '{dbItem.Id}' does not exist");
            _db.Entry(dbItem).CurrentValues.SetValues(item);
            await _db.SaveChangesAsync();
        }

    }
}
