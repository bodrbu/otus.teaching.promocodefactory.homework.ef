﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [MaxLength(200)]
        public string Name { get; set; }
        public string Description { get; set; }

        public IList<CustomerPreference> CustomerPreferences { get; set; }
    }
}