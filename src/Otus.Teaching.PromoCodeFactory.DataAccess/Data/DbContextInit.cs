﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{

    public class DbContextInit : IAppDBContextInit
    {
        private readonly AppDBContext _db;

        public DbContextInit(AppDBContext db)
        {
            _db = db;
        }

        public void AppDbContextInit()
        {
            // The real DB already exist
            //_db.Database.EnsureDeleted();
            _db.Database.EnsureCreated();
            
            // The real DB already exist
            //_db.AddRange(FakeDataFactory.Employees);
            //_db.AddRange(FakeDataFactory.Preferences);
            //_db.AddRange(FakeDataFactory.Customers);
            _db.SaveChanges();
        }
    }
}
