﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRep;
        private readonly IRepository<Preference> _preferenceRep;
        private readonly IRepository<PromoCode> _promoCodeRep;

        public CustomersController(        
            IRepository<Customer> customerRep,
            IRepository<Preference> preferenceRep,
            IRepository<PromoCode> promoCodeRep
        )
        {
            _customerRep = customerRep;
            _preferenceRep = preferenceRep;
            _promoCodeRep = promoCodeRep;
        }

        /// <summary>
        /// Get a list of customers
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRep.GetAllAsync();
            var customersToShort = customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }
            ).ToList();
            return Ok(customersToShort);
        }

        /// <summary>
        /// Get a customer by ID
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRep.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }
            var customerResponse = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };

            customerResponse.Preferences = customer.CustomerPreferences.Select(x => new PreferenceResponse()
            {
                Id = x.PreferenceId,
                Name = x.pre.Name
            }).ToList();

            var promoCodeResponse = customer.Promocodes
                .Select(promo => new PromoCodeShortResponse()
                {
                    Id = promo.Id,
                    Code = promo.Code,
                    PartnerName = promo.PartnerName,
                    ServiceInfo = promo.ServiceInfo,
                    BeginDate = promo.BeginDate.ToString("yyyy.MM.dd HH:mm:ss"),
                    EndDate = promo.EndDate.ToString("yyyy.MM.dd HH:mm:ss")
                }
                ).ToList();
            customerResponse.PromoCodes = promoCodeResponse;

            return Ok(customerResponse);            
        }

        /// <summary>
        /// Create a new customer
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRep.GetListByListId(request.PreferenceIds);
            
            var customer = new Customer 
            {
                Id = Guid.NewGuid(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };

            if (preferences != null && preferences.Count() > 0)
            {
                customer.CustomerPreferences = preferences.Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    Preference = x
                }).ToList();            
            }
            
            await _customerRep.CreateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Updating customer data
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRep.GetByIdAsync(id);

            if (customer == null) return NotFound();

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;

            var preferences = await _preferenceRep.GetListByListId(request.PreferenceIds);

            if (preferences != null && preferences.Count() > 0)
            {
                customer.CustomerPreferences = preferences.Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    Preference = x
                }).ToList();
            }

            await _customerRep.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Deleting a customer
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRep.GetByIdAsync(id);

            if (customer == null) return NotFound();

            await _customerRep.DeleteAsync(id);

            return Ok();
        }
    }
}