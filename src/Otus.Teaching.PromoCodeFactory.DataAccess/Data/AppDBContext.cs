﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
     public class AppDBContext: DbContext
    {
        public DbSet<Employee> employees { get; set; }
        public DbSet<Role> roles { get; set; }
        public DbSet<Customer> customers { get; set; }
        public DbSet<Preference> preferences { get; set; }
        public DbSet<PromoCode> promoCodes { get; set; }

        public AppDBContext(DbContextOptions<AppDBContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>().ToTable("CustomerPreferences").HasKey(bc => new { bc.CustomerId, bc.PreferenceId });

        }

    }
}
